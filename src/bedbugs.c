/*
 * Output bits of a rule file for Golly for a Conway-like cellular
 * automaton displayed with smoothing reminiscent of the Mullard
 * SAA5050 Teletext character generation (aka BBC "MODE 7").
 *
 * The resulting automaton flips the world between two states:
 * one where everything is one of two states, and one where an
 * expanded set of 32 states is used (16 'off', 16 'on') according
 * to what diagonals are required for the smoothing. Each of the
 * expanded states has an associated icon.
 *
 * In the first step we go from two states to 32 implementing the
 * smoothing algorithm as a transition function on a 3x3 neighbourhood.
 * In the second step we go from 32 back to 2 with a Conway-derived
 * transition function, again on a 3x3 neighbourhood (this step is not
 * in this source file).
 * (FIXME: this would be more slick, although probably even less
 * efficient, implemented in one go with a 5x5 neighbourhood, which
 * Golly does support.)
 *
 * The smoothing algorithm and graphics are derived from 'Bedstead' by
 * Ben Harris: http://bjh21.me.uk/bedstead/
 * The idea of applying this smoothing to Conway's Life can be blamed
 * on Simon Tatham.
 */

/*
 * This file is part of Bedbugs.
 * Copyright (C) 2014 Jacob Nevins.
 * Portions copyright (C) 2009 Ben Harris (see below).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Website: http://www.chiark.greenend.org.uk/ucgi/~jacobn/git/bedbugs.git/
 *
 *
 * This source file, bedbugs.c, is based on
 * http://bjh21.me.uk/bedstead/  bedstead.c  bzr r137
 * from which the following bits of the copyright notice and documentation
 * remain relevant:
 *
 * [...] the file is covered by the following:
 *
 * Copyright (c) 2009 Ben Harris.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This [was once] a program to construct an outline font from a bitmap.
 * It's based on the character-rounding algorithm of the Mullard SAA5050
 * series of Teletext character generators, and thus works best on
 * character shapes in the same style of those of the SAA5050.  This
 * file includes all of the glyphs from the SAA5050, SAA5051, SAA5052,
 * SAA5053, SAA5054, SAA5055, SAA5056, and SAA5057.  The output is a
 * Spline Font Database file suitable for feeding to Fontforge.
 *
 * The character-smoothing algorithm of the SAA5050 and friends is
 * a fairly simple means of expanding a 5x9 pixel character to 10x18
 * pixels for use on an interlaced display.  All it does is to detect
 * 2x2 clumps of pixels containing a diagonal line and add a couple of
 * subpixels to it, like this:
 *
 * . #  -> . . # # -> . . # # or # . -> # # . . -> # # . .
 * # .     . . # #    . # # #    . #    # # . .    # # # .
 *         # # . .    # # # .           . . # #    . # # #
 *         # # . .    # # . .           . . # #    . . # #
 *
 * This is applied to every occurrence of these patterns, even when
 * they overlap, and the result is that thin diagonal lines are
 * smoothed out while other features mostly remain the same.
 *
 * One way of extending this towards continuity would be to repeatedly
 * double the resolution and add more pixels to diagonals each time,
 * but this ends up with the diagonals being much too heavy.  Instead,
 * in places where the SAA5050 would add pixels, this program adds a
 * largeish triangle to each unfilled pixel, and removes a small
 * triangle from each filled one, something like this:
 *
 * . #  -> . . # # -> . . / # or # . -> # # . . -> # \ . .
 * # .     . . # #    . / # /    . #    # # . .    \ # \ .
 *         # # . .    / # / .           . . # #    . \ # \
 *         # # . .    # / . .           . . # #    . . \ #
 * 
 * The position of the lines is such that on a long diagonal line, the
 * amount of filled space is the same as in the rounded bitmap.  There
 * are a few additional complications, in that the trimming of filled
 * pixels can leave odd gaps where a diagonal stem joins another one,
 * so the code detects this and doesn't trim in these cases:
 *
 * . # # -> . . # # # # -> . . / # # # -> . . / # # #
 * # . .    . . # # # #    . / # / # #    . / # # # #
 *          # # . . . .    / # / . . .    / # / . . .
 *          # # . . . .    # / . . . .    # / . . . .
 *
 * [...]
 */

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static bool *
blank(int size)
{
    return calloc(size*size, sizeof(bool));
}

static void output(bool *r, int size) {
    int x, y;
    for (y=0; y<size; y++) {
        putchar('\"');
        for (x=0; x<size; x++) {
            putchar(r[x+y*size] ? 'A' : '.');
        }
        putchar('\"');
        putchar('\n');
    }
}

static bool *
triangle(bool *r, int size, int startpos, bool top, bool left)
{
    int x, y;
    for (y = 0; y < startpos; y++) {
        for (x = 0; x < startpos-y; x++) {
            int rx, ry;
            if (top) {
                ry = y;
            } else {
                ry = size-1 - y;
            }
            if (left) {
                rx = x;
            } else {
                rx = size-1 - x;
            }
            r[rx+ry*size]=true;
        }
    }
    return r;
}

static void
triangles(bool *r, int size, int startpos, int bl, int br, int tr, int tl)
{
    if (!bl) {
        triangle(r, size, startpos, false, true);
    }
    if (!br) {
        triangle(r, size, startpos, false, false);
    }
    if (!tr) {
        triangle(r, size, startpos, true, false);
    }
    if (!tl) {
        triangle(r, size, startpos, true, true);
    }
}

static void
whitepixel(bool *r, int size, int bl, int br, int tr, int tl)
{
    /* wrt blackpixel(): -1 for adjacency, -1 for gridlines
     * (which Golly has no apparent way to suppress :/ ) */
    const int startpos = size-size/4-2;
    triangles(r, size, startpos, bl, br, tr, tl);
}

static void
blackpixel(bool *r, int size, int bl, int br, int tr, int tl)
{
    const int startpos = size/4;
    int i;
    triangles(r, size, startpos, bl, br, tr, tl);
    for (i=0; i<size*size; i++) {
        r[i] = !r[i];
    }
}

void bedstead (void)
{
        /* Output smoothing rules as a Golly rule table mapping from
         * (0,1) to an expanded set of states that we can hang icons
         * from. (The smoothing algorithm requires a 3x3 neighbourhood,
         * which is fortuitous.) */

        /* Index into small bitmap */
#define GETPIX(x,y) (!!(iter & 1u<<((y)*3+(x))))
#define L GETPIX(x-1, y)
#define R GETPIX(x+1, y)
#define U GETPIX(x, y-1)
#define D GETPIX(x, y+1)
#define UL GETPIX(x-1, y-1)
#define UR GETPIX(x+1, y-1)
#define DL GETPIX(x-1, y+1)
#define DR GETPIX(x+1, y+1)

        /* Iterate over all neighbourhoods (9 cells) */
        int iter;
        for (iter = 0; iter < 1u<<9; iter++) {
                        int state, x = 1, y = 1;
                        /* The core of the Bedstead smoothing algorithm
                         * from Ben Harris. */
			if (GETPIX(x, y)) {
				bool tl, tr, bl, br;

				/* Assume filled in */
				tl = tr = bl = br = true;
				/* Check for diagonals */
				if ((UL && !U && !L) || (DR && !D && !R))
					tr = bl = false;
				if ((UR && !U && !R) || (DL && !D && !L))
					tl = br = false;
				/* Avoid odd gaps */
				if (L || UL || U) tl = true;
				if (R || UR || U) tr = true;
				if (L || DL || D) bl = true;
				if (R || DR || D) br = true;
                                /* On states are 18..33 */
                                state = 2 + 16 + (bl | br<<1 | tr<<2 | tl<<3);
			} else {
				bool tl, tr, bl, br;

				/* Assume clear */
				tl = tr = bl = br = false;
				/* white pixel -- just diagonals */
				if (L && U && !UL) tl = true;
				if (R && U && !UR) tr = true;
				if (L && D && !DL) bl = true;
				if (R && D && !DR) br = true;
                                /* Off states are 2..17 */
                                state = 2 + (bl | br<<1 | tr<<2 | tl<<3);
			}
                        if (iter == 0) {
                                /* Special case: we don't want to try to
                                 * flip all of infinite empty space between
                                 * 0 and 2 every step (this has odd effects
                                 * on Golly) */
                                assert(state == 2);
                                state = 0;
                        }
                        /* Output rule in Golly rule format
                         * (FIXME completely unoptimised) */
                        printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
                               GETPIX(x,y),U,UR,R,DR,D,DL,L,UL,state);
        }
}

void icons(void)
{
        /* Bedbugs icons. In each size supported by Golly,
         * draw one icon per state, in the XPM format Golly wants.
         * This does not actually depend on bedstead() at all.
         * (FIXME: but it could. Some of the states we generate icons
         * and rules for are not actually reachable. We could spot
         * unused states above and avoid generating icons or rules for
         * them.)
         * (FIXME: we could also output pre-Golly-2.5 format icons) */
        {
            const int sizes[] = {7, 15, 31};
            int size_index;
            for (size_index = 0;
                 size_index < sizeof(sizes)/sizeof(sizes[0]);
                 size_index++) {
                int size = sizes[size_index], state;
                printf("XPM\n/* width height num_colors chars_per_pixel */\n");
                printf("\"%d %d 2 1\"\n", size, size*33);
                /* We have to have a non-greyscale colour to trigger Golly's
                 * "multi-colour icon" mode, so that we can make some states'
                 * non-icon versions black. And it's not sufficient to
                 * include an unreferenced, colour, so our 'on' state is
                 * off-white. */
                printf("/* colors */\n\". c #000000\"\n\"A c #FEFEFF\"\n");
                /* icons never used for state 0 */
                for (state = 1; state < 34; state++) {
                    bool *r = blank(size);
                    if (state == 0) {
                        ; /* Conway 'off': nothing */
                    } else if (state == 1) {
                        /* Conway 'on': everything */
                        memset(r, 1, size*size*sizeof(*r));
                    } else if (state < 16+2) {
                        /* Bedstead 'off' */
                        int bits = ~(state-2);
                        whitepixel(r, size, bits&1, bits&2, bits&4, bits&8);
                    } else {
                        /* Bedstead 'on' */
                        int bits = state-16-2;
                        blackpixel(r, size, bits&1, bits&2, bits&4, bits&8);
                    }
                    printf("/* icon for state %d */\n", state);
                    output(r, size);
                    free(r);
                }
            }
        }
}

void colours(void)
{
    int i;
    for (i=0; i<34; i++) {
        int lvl;
        switch (i) {
            case 0:
                lvl = 0;
                break;
            case 1:
                lvl = 255;
                break;
            default:
                lvl = (i >= 2+16) ? 255 : 0;
                break;
        }
        printf("%2d %3d %3d %3d\n", i, lvl, lvl, lvl);
    }
}

int main(int argc, char *argv[])
{
    while (!feof(stdin)) {
        char l[100]; /* bleah */
        if (!fgets(l, sizeof(l), stdin)) {
            break;
        }
        if (strlen(l) == sizeof(l)-1) {
            fprintf(stderr, "Long input line; possible truncation; I suck\n");
            exit(1);
        }
        if (strlen(l) >= 2 && l[0] == '@' && l[1] == '@') {
            size_t n = strcspn(l+2, "\n");
            if (strncmp(l+2, "BEDSTEAD", n) == 0) {
                bedstead();
            } else if (strncmp(l+2, "ICONS", n) == 0) {
                icons();
            } else if (strncmp(l+2, "COLORS", n) == 0) {
                colours();
            } else {
                /* Bodily insert named file on stdout. */
                FILE *f;
                l[n+2]='\0';
                if (!(f = fopen(l+2, "r"))) {
                    fprintf(stderr, "Couldn't open '%s': %s\n", l+2,
                            strerror(errno));
                    exit(1);
                } else {
                    do {
                        char buf[2048]; /* also bleah */
                        size_t n = fread(buf, 1, sizeof(buf), f);
                        fwrite(buf, 1, n, stdout);
                    } while (!feof(f) && !ferror(f));
                    if (ferror(f)) {
                        fprintf(stderr, "Error reading '%s': %s\n", l+2,
                                strerror(errno));
                        exit(1);
                    }
                    fclose(f);
                }
            }
        } else {
            printf("%s", l);
        }
    }
    return 0;
}
